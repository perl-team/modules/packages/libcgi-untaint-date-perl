libcgi-untaint-date-perl (1.00-4) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Damyan Ivanov ]
  * change Priority from 'extra' to 'optional'

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 10 Jun 2022 15:16:06 +0100

libcgi-untaint-date-perl (1.00-3.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 08 Jan 2021 13:21:09 +0100

libcgi-untaint-date-perl (1.00-3) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ gregor herrmann ]
  * debian/control: split Build-Depends{,-Indep}.

  [ Ben Hutchings ]
  * Remove myself from Uploaders

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Switch to source format "3.0 (quilt)".
  * Add explicit (build) dependency on libcgi-pm-perl.
  * Bump debhelper compatibility level to 9, and dh(1) in debian/rules.
  * debian/copyright: switch formatting to Copyright-Format 1.0.
  * Update short and long description.
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Sat, 13 Jun 2015 13:43:50 +0200

libcgi-untaint-date-perl (1.00-2) unstable; urgency=low

  [ Damyan Ivanov ]
  * Take over for the Debian Perl Group on maintainer's
    request
    http://lists.debian.org/debian-perl/2008/01/msg00262.html
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed:
    Homepage pseudo-field (Description). Changed: Maintainer set to
    Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
    (was: Ben Hutchings <ben@decadent.org.uk>); Ben Hutchings
    <ben@decadent.org.uk> moved to Uploaders.
  * added debian/watch

  [ Gunnar Wolf ]
  * Bumped up standards-version to 3.7.3
  * Added myself as an uploader

 -- Gunnar Wolf <gwolf@debian.org>  Mon, 12 May 2008 14:11:46 -0500

libcgi-untaint-date-perl (1.00-1) unstable; urgency=low

  * Initial release - closes: #432506

 -- Ben Hutchings <ben@decadent.org.uk>  Wed, 11 Jul 2007 00:23:16 +0100
